Lita.configure do |config|
  # The name your robot will use.
  config.robot.name = "TFX"

  # The locale code for the language to use.
  # config.robot.locale = :en

  # The severity of messages to log. Options are:
  # :debug, :info, :warn, :error, :fatal
  # Messages at the selected level and above will be logged.
  config.robot.log_level = :info

  # An array of user IDs that are considered administrators. These users
  # the ability to add and remove other users from authorization groups.
  # What is considered a user ID will change depending on which adapter you use.
  config.robot.admins = ["U26Q34YCU"]

  # The adapter you want to connect with. Make sure you've added the
  # appropriate gem to the Gemfile.
  config.robot.adapter = :slack
  config.adapters.slack.token = "xoxb-82455593751-s5V6v2zN8TT0SOZ5FNV0AKsv"

  ## Example: Set options for the chosen adapter.
  # config.adapter.username = "myname"
  # config.adapter.password = "secret"

  ## Example: Set options for the Redis connection.
  config.redis[:host] = "127.0.0.1"
  config.redis[:port] = 6379

  ## Example: Set configuration for any loaded handlers. See the handler's
  ## documentation for options.
  # config.handlers.some_handler.some_config_key = "value"

  #config.handlers.capistrano.server = "78.47.45.48"
  #config.handlers.capistrano.server_user = "design"
  #config.handlers.capistrano.server_password = "MdEJs2lcylRmtG-uWETFwP6xA8Rk_XVV"
  #config.handlers.capistrano.slack_api_token = "xoxb-82455593751-s5V6v2zN8TT0SOZ5FNV0AKsv"  # not required, if not using Slack reminders

  config.handlers.cap_deploy.apps = {
    'design_tfx': {
       subdir: "design",
       git: 'git@bitbucket.org:taxesforexpats/devops.git',
       install_mode: "capistrano",
       envs: {
        'staging': {branch: 'master', ssh_command: "ssh ", auth_group: 'designinstall'},
        'html': {branch: 'master', ssh_command: "ssh taxesfor@", install_mode: "ssh", auth_group: 'designinstall'}
       }
    }, # this will use "production" as rails env and using "master" branch
    'app2': {
      git: 'git@git.example.com:account/app2.git',
      envs: {
        'production': 'master',
        'staging': 'develop',
      }
    },
  }

#config.handlers.capistrano.deploy_tree = {
#  design_tfx: {
#    staging: {
#      dir: "/home/cpamonitor/slack",
#      auth_group: "design", # auth_group required to be able to deploy
#      channel: "app_deployed", # not required, if configured limits deploys to this channel
#      envs: [
#        "staging",
#        "qa2"
#      ]
#    }
#  },
#}





end
