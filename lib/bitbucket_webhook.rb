require 'file_logger'
require 'lita'

module BitbucketWebhook

  def process_bitbucket(request, response)
    json_data = parse_json(request.body) or return
    result = conditions_parse(json_data)

    target = Lita::Source.new(room: 'auto_deploy')
    response_back = ->(msg) { robot.send_message(target, msg) }

    if result[:repo] == 'taxesforexpats/taxesforexpats' && result[:branch] == 'staging' &&
        result[:state] == 'SUCCESSFUL'
      response_back.call('Starting deploy code for UAT')
      deploy_app('test_server', 'staging', nil, response_back,
                branch: 'staging', port: 8088, drop_db: false,
                user_name: 'Bitbucket', target: target)
    elsif result[:repo] == 'taxesforexpats/taxdome' && result[:branch] == 'develop' &&
        result[:state] == 'SUCCESSFUL'
      response_back.call('Starting deploy staging for TaxDome test server')
      deploy_app('test_td', 'staging', nil, response_back, branch: 'develop', drop_db: false,
                user_name: 'Bitbucket', target: target)
    elsif result[:repo] == 'taxesforexpats/taxdome' && result[:branch] == 'master' &&
        result[:state] == 'SUCCESSFUL'
      response_back.call('Starting deploy production for TaxDome test server')
      deploy_app('test_td', 'production', nil, response_back, branch: 'master', drop_db: false,
                user_name: 'Bitbucket', target: target)
    end
  end

  private

  def parse_json(json)
    MultiJson.load(json)
  rescue MultiJson::LoadError => e
    Lita.logger.error("Could not parse JSON from Bitbucket: #{e.message}")
    return
  end

  def conditions_parse(json)
    result = Hash.new

    result[:repo] = json["repository"]["full_name"]
    result[:branch] = json["commit_status"]["refname"]
    result[:state] = json["commit_status"]["state"]

    Lita.logger.warn result.inspect
    result
  end
end
