require 'fog/config'
require 'fog/storage'

class FileLogger

  attr_reader :is_start

  def initialize(log, filename)
    @log = log
    @filename = filename
    start
  end

  def start
    @file = Tempfile.new('slack_project_log')
    @is_start = true
  end

  def flush
    @file.flush if @is_start
  end

  def finish
    if @is_start
      @file.flush
      @file.close
      return move_to_fog
    end
  ensure
    @file.delete if is_start
    @is_start = false
  end

  def write(line)
    @file.write(line) if is_start
  end

  def debug(msg)
    write msg
    @log.debug(msg)
  end

  def info(msg)
    write msg
    @log.info(msg)
  end

  def finish_log_file
  end

  private

  def move_to_fog
    Storage::Base.new.store(@file.path, @filename)
  end
end

#def_delegators :@log, :unknown
#def_delegators :@log, :fatal
#def_delegators :@log, :error
#def_delegators :@log, :warn
#def_delegators :@log, :info
#def_delegators :@log, :debug
