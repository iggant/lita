module DefineDynamicRoutes

  # define route for each rapporteur
  def define_dinamic_routes
    config.apps.each do |app, app_config|
      # define command "deploy APP"
      self.class.route(
        %r{deploy (#{app})$},
        :deploy_production_for_app,
        command: true,
        # restrict_to: [:admins, value[:deploy_group]],
        help: { "deploy #{app}" => "deploy produciton for #{app}"}
      )

      # define command "deploy ENV for APP"
      # puts "define route: ^deploy\s+(#{app})\s+(#{area})\s+(.+)\s+(.+)"
      envs = app_config[:envs] ? app_config[:envs].keys : ['production']
      self.class.route(
        %r{deploy +(\w+) +for +(#{app})$},
        :deploy_env_for_app,
        command: true,
        # restrict_to: [:admins, value[:deploy_group]],
        help: { "deploy #{envs.join("|")} for #{app}" => "deploy ENV for #{app}"}
      )

      # define command "deploy APP BRANCH= PORT="
      self.class.route(
        %r{deploy +(\w+) +for +(#{app}) +BRANCH=([\w|\/|\-|\.]+) +PORT=(\w+)( drop_db)?$},
        :deploy_branch_for_app,
        command: true,
        # restrict_to: [:admins, value[:deploy_group]],
        help: { "deploy #{envs.join("|")} for #{app} BRANCH PORT DROP_DB" => "deploy ENV for #{app} BRANCH=develop PORT=9204 drop_db"}
      )

      self.class.route(
        %r{server +restart +for +(#{app}) +PORT=(\w+)$},
        :restart_server_branch_for_app,
        command: true,
        # restrict_to: [:admins, value[:deploy_group]],
        help: {"server restart for #{app} PORT" => "server restart for #{app} PORT=9204"}
      )
    end
  end

end
