module UserAuths

  def define_user_routes
    self.class.route(
      %r{^deploy\s+auth\s+list},
      :deploy_auth_list,
      command: true,
      help: { "deploy auth list [APP] " => "List required auth groups to deploy" }
    )
    self.class.route(
      %r{^show\s+current_user},
      :show_current_user,
      command: true,
      help: { "show current_user " => "Show current user mention name" }
    )
    self.class.route(
      %r{^auth\s+add\s+(\w+)\s+(\w+)},
      :add_user_auth_group,
      command: true,
      help: { "auth add [USER] [AUTHGROUP] " => "Add user to authgroup" }
    )
    self.class.route(
      %r{^auth\s+remove\s+(\w+)\s+(\w+)},
      :remove_user_auth_group,
      command: true,
      help: { "auth remove [USER] [AUTHGROUP] " => "Remove user to authgroup" }
    )
    self.class.route(
      %r{^auth\s+show\s+(\w+)},
      :show_auth_group,
      command: true,
      help: { "auth show [AUTHGROUP] " => "Show all users in authgroup" }
    )
  end

  def show_current_user(response)
    response.reply_privately('Your name:')
    response.reply_privately(response.user.mention_name)
  end

  def deploy_auth_list(response)
    requested_app = response.args[2]
    if requested_app.nil?
      apps_auth_tree = get_apps_auth_groups(config.apps)
      response.reply_privately("Auth groups for apps:\n#{apps_auth_tree}")
    else
      app_tree = get_app_auth_groups(config.apps[requested_app.to_sym])
      response.reply_privately("Auth group needed to deploy #{requested_app}: \n #{app_tree}")
    end
  end

  def add_user_auth_group(response)
    user_name = response.args[1]
    auth_group = response.args[2]
    if robot.config.robot.admins.include?(response.user.id)
      redis.sadd("usergroup:#{auth_group}", user_name)
    else
      failure = 'Your are not admin to add user for the group'
      log.error failure
      response.reply(failure)
    end
  end

  def remove_user_auth_group(response)
    user_name = response.args[1]
    auth_group = response.args[2]
    if robot.config.robot.admins.include?(response.user.id)
      redis.srem("usergroup:#{auth_group}", user_name)
    else
      failure = 'Your are not admin to remove user for the group'
      log.error failure
      response.reply(failure)
    end
  end

  def show_auth_group(response)
    auth_group = response.args[1]
    users = redis.smembers("usergroup:#{auth_group}").map { |user| user }.join(',')
    response.reply(users)
  end

  protected

  def is_user_in_group(user_name, auth_group)
    log.debug redis.smembers("usergroup:#{auth_group}")
    redis.smembers("usergroup:#{auth_group}").include?(user_name)
  end

  private

  def get_apps_auth_groups(apps)
    app_tree = {}
    apps.each do |app, value|
      app_tree[app.to_s] = get_app_auth_groups(value)
    end
    app_tree.flatten.join("\n")
  end

  def get_app_auth_groups(app)
    app[:envs].map { |stage, env| "#{stage}: #{env[:auth_group]}" }.join("\n")
  end
end
