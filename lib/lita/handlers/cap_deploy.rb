require 'utils'
require 'step_flow'
require 'slack'
require 'define_static_routes'
require 'define_dynamic_routes'
require 'user_auths'
require 'deploy'
require 'bitbucket_webhook'

module Lita
  module Handlers
    class CapDeploy < Handler
      include StepFlow
      include DefineStaticRoutes
      include DefineDynamicRoutes
      include UserAuths
      include Deploy
      include Utils
      include BitbucketWebhook

      # example:
      # config.handlers.cap_deploy.apps = {
      #   'app1' => {
      #     dir: ''
      #     shell: 'ssh username@ip -pPORT -e "" '
      #     git: 'git@git.example.com:account/app1.git',
      #     channel: 'room_name'
      #   }, # this will use "production" as rails env and using "master" branch
      #   'app2' => {
      #     git: 'git@git.example.com:account/app2.git',
      #     channel: 'room_name'
      #       stages: {
      #         'production': {
      #             auth_group: 'some_group'
      #         },
      #         'staging': {,
      #             auth_group: 'some_group'
      #         }
      #       }
      #       envs: {
      #         'production' => 'master',
      #         'staging' => 'develop',
      #       }
      #   },
      # }
      config :apps, type: Hash, required: true
      config :log_file, type: String, required: false

      http.post "/bitbucket-webhook", :process_bitbucket

      on :loaded, :define_routes

      def define_routes(payload)
        define_static_routes
        define_dinamic_routes
        define_user_routes
      end

      # command: "deploy #{env} for #{app}"
      def deploy_env_for_app(response)
        env = response.matches[0][0]
        app = response.matches[0][1]

        user_name = response.user.mention_name
        target = response.message.source.room_object
        response_back = ->(msg) { response.reply(msg) }

        deploy_app(app, env, nil, response_back, user_name: user_name, target: target)
      end

      def deploy_branch_for_app(response)
        env = response.matches[0][0]
        app = response.matches[0][1]

        branch = response.matches[0][2]
        port = response.matches[0][3]

        drop_db = !!response.matches[0][4]

        user_name = response.user.mention_name
        target = response.message.source.room_object
        response_back = ->(msg) { response.reply(msg) }

        deploy_app(app, env, nil, response_back, branch: branch, port: port, drop_db: drop_db,
                   user_name: user_name, target: target)
      end

      def restart_server_branch_for_app(response)
        app = response.matches[0][0]
        port = response.matches[0][1]

        user_name = response.user.mention_name
        target = response.message.source.room_object
        response_back = ->(msg) { response.reply(msg) }

        deploy_app(app, 'server_restart', nil, response_back, port: port, user_name: user_name, target: target)
      end

      # command: "deploy #{app}"
      def deploy_production_for_app(response)
        env = 'production'
        app = response.matches[0][0]

        user_name = response.user.mention_name
        target = response.message.source.room_object
        response_back = ->(msg) { response.reply(msg) }

        deploy_app(app, env, nil, response_back, user_name: user_name, target: target)
      end

      private

      def verbose
        # config.scm_verbose ? nil : "-q"
      end

      Lita.register_handler(self)
    end
  end
end
