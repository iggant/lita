module DefineStaticRoutes

  def define_static_routes
    self.class.route(
      %r{deploy\s+list},
      :deploy_list_apps,
      command: true,
      help: { "deploy list" => "List available apps for deploy"}
    )
  end

  # command: "deploy list"
  def deploy_list_apps(response)
    response.reply_privately('Available apps:')
    apps = config.apps.map do |app, app_config|
      envs = app_config[:envs] ? app_config[:envs].keys.join(",") : 'production'
      "#{app}(#{envs})"
    end
    response.reply_privately(apps)
  end
end
