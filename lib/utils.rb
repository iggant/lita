require 'open3'

# Use for
module Utils
  class Service

    def self.run_in_dir(cmd, dir, log)
      lita_mark = "LITA=#{Lita::VERSION}"
      _cmd = "cd #{dir} && #{lita_mark} #{cmd}"
      # log.info _cmd
      # running bundle install inside a bundle-managed shell to avoid "RuntimeError: Specs already loaded"
      # see https://github.com/bundler/bundler/issues/1981#issuecomment-6292686
      Bundler.with_clean_env do
        log.debug "execute: #{cmd}"
        # system(_cmd) # can't catch output
        Utils::Service.execute_command(_cmd, log)
      end
    end

    def gen_deploy_msg(title, color, body, user, app, area, env, tag)
      msg = Adapters::Slack::Attachment.new(
        body,
        title: "Deploy - #{title}",
        color: "#{color}",
        pretext: "@#{user}:",
        fields: [
          {
            title: "App",
            value: app,
            short: true
          },
          {
            title: "Area",
            value: area,
            short: true
          },
          {
            title: "Environment",
            value: env,
            short: true
          },
          {
            title: "tag",
            value: tag,
            short: true
          },
        ]
      )
    end

    def allowed_room?(room_id, allowed_channel, log)
      room = Lita::Room.find_by_id(room_id)
      log.debug "Try to check Lita room allowed room_id: #{room_id} which is #{room.inspect} with allowed_channel: #{allowed_channel}"
      return false if room.nil?
      return true if room.metadata["name"] == allowed_channel
    end

    protected

    # execute commandline and catch stdout and stderr
    def self.execute_command(cmd, log)
      full_stdout = String.new
      full_stderr = String.new
      started_at  = Time.now

      Open3.popen3(cmd) do |stdin, stdout, stderr, wait_thr|
        stdout_thread = Thread.new do
          while (line = stdout.gets) do
            full_stdout += line
            log.write(line)
            # printf line
          end
        end

        stderr_thread = Thread.new do
          while (line = stderr.gets) do
            full_stderr += line
            log.write(line)
          end
        end

        stdout_thread.join
        stderr_thread.join

        exit_status = wait_thr.value.to_i
        finished_at = Time.now
        runtime     = finished_at - started_at

        {
          cmd:          cmd,
          started_at:   started_at,
          finished_at:  finished_at,
          exit_status:  exit_status,
          runtime:      runtime,
          stdout:       full_stdout,
          stderr:       full_stderr,
        }
      end
    end

  end
end
