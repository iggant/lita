require 'singleton'
require 'active_model'

module Storage
  class Config
    include Singleton
    include ::ActiveModel::Validations

    class Invalid < StandardError; end

    attr_accessor :fail_silently
    attr_accessor :log_silently
    attr_accessor :always_upload
    attr_accessor :ignored_files
    attr_accessor :enabled
    attr_accessor :custom_headers
    attr_accessor :cdn_distribution_id

    # FOG configuration
    attr_accessor :fog_provider          # Currently Supported ['AWS', 'Rackspace']
    attr_accessor :fog_directory         # e.g. 'the-bucket-name'
    attr_accessor :file_directories
    attr_accessor :fog_region            # e.g. 'eu-west-1'
    attr_accessor :asset_directory

    # Amazon AWS
    attr_accessor :aws_access_key_id, :aws_secret_access_key, :aws_reduced_redundancy

    # Rackspace
    attr_accessor :rackspace_username, :rackspace_api_key, :rackspace_auth_url

    # Google Storage
    attr_accessor :google_storage_secret_access_key, :google_storage_access_key_id

    validates :fog_provider,          :presence => true
    validates :fog_directory,         :presence => true

    validates :aws_access_key_id,     :presence => true, :if => :aws?
    validates :aws_secret_access_key, :presence => true, :if => :aws?
    validates :rackspace_username,    :presence => true, :if => :rackspace?
    validates :rackspace_api_key,     :presence => true, :if => :rackspace?
    validates :google_storage_secret_access_key,  :presence => true, :if => :google?
    validates :google_storage_access_key_id,      :presence => true, :if => :google?

    def initialize
      self.fog_region = nil
      self.fail_silently = false
      self.log_silently = true
      self.always_upload = []
      self.ignored_files = []
      self.custom_headers = {}
      self.enabled = true
      self.cdn_distribution_id = nil
      self.file_directories = {}
      load_yml! if yml_exists?
    end

    def aws?
      fog_provider == 'AWS'
    end

    def aws_rrs?
      aws_reduced_redundancy == true
    end

    def fail_silently?
      fail_silently || !enabled?
    end

    def enabled?
      enabled == true
    end

    def rackspace?
      fog_provider == 'Rackspace'
    end

    def google?
      fog_provider == 'Google'
    end

    def yml_exists?
      File.exists?(self.yml_path)
    end

    def yml
      @yml ||= YAML.load_file(yml_path)['storage']
    end

    def yml_path
      'storage.yml'
    end

    def load_yml!
      self.enabled                = yml["enabled"] if yml.has_key?('enabled')
      self.fog_provider           = yml["fog_provider"]
      self.fog_directory          = yml["fog_directory"]
      self.file_directories       = yml["file_directories"]
      self.asset_directory        = yml["asset_directory"]
      self.fog_region             = yml["fog_region"]
      self.aws_access_key_id      = yml["aws_access_key_id"]
      self.aws_secret_access_key  = yml["aws_secret_access_key"]
      self.aws_reduced_redundancy = yml["aws_reduced_redundancy"]
      self.rackspace_username     = yml["rackspace_username"]
      self.rackspace_auth_url     = yml["rackspace_auth_url"] if yml.has_key?("rackspace_auth_url")
      self.rackspace_api_key      = yml["rackspace_api_key"]
      self.google_storage_secret_access_key = yml["google_storage_secret_access_key"]
      self.google_storage_access_key_id     = yml["google_storage_access_key_id"]
      self.fail_silently          = yml["fail_silently"] if yml.has_key?("fail_silently")
      self.always_upload          = yml["always_upload"] if yml.has_key?("always_upload")
      self.ignored_files          = yml["ignored_files"] if yml.has_key?("ignored_files")
      self.custom_headers          = yml["custom_headers"] if yml.has_key?("custom_headers")
      self.cdn_distribution_id    = yml['cdn_distribution_id'] if yml.has_key?("cdn_distribution_id")
    end

    def fog_options
      options = { :provider => fog_provider }
      if aws?
        options.merge!({
          :aws_access_key_id => aws_access_key_id,
          :aws_secret_access_key => aws_secret_access_key
        })
      elsif rackspace?
        options.merge!({
          :rackspace_username => rackspace_username,
          :rackspace_api_key => rackspace_api_key
        })
        options.merge!({
          :rackspace_region => fog_region
        }) if fog_region
        options.merge!({ :rackspace_auth_url => rackspace_auth_url }) if rackspace_auth_url
      elsif google?
        options.merge!({
          :google_storage_secret_access_key => google_storage_secret_access_key,
          :google_storage_access_key_id => google_storage_access_key_id
        })
      else
        raise ArgumentError, "Unknown provider: #{fog_provider} only AWS and Rackspace are supported currently."
      end

      options.merge!({:region => fog_region}) if fog_region && !rackspace?
      return options
    end
  end
end
