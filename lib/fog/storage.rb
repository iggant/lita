require 'singleton'
require 'fog/config'
require 'fog'

module Storage
  class Base

    def store(file_path, cloud_file_path)
      file = {
        key: cloud_file_path,
        body: File.open(file_path, 'r'),
        public: true
      }
      if config.aws? && config.aws_rrs?
        file.merge!({
          :storage_class => 'REDUCED_REDUNDANCY'
        })
      end
      file = bucket.files.create(file)
      generate_public_url(file)
    end

    def generate_public_url(file)
      #connection.directories.new(key: get_fog_directory).files.new(key: file.key).url(Time.now+86400)
      file.public_url
    end

    @@config = nil
    @@connection = nil
    @@bucket = nil

    def connection
      @@connection ||= Fog::Storage.new(config.fog_options)
    end

    def config
      @@config ||= Storage::Config.instance
    end

    def get_fog_directory
      config.fog_directory
    end

    def bucket
      @@bucket ||= connection.directories.get( get_fog_directory )
    end
  end
end
