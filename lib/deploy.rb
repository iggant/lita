require 'file_logger'

module Deploy

  # If a deploy is in progress the deploy_tracker handler will return a
  # reponse to chat and will interrupt further using the interrupt_deploy
  # method
  def deploy_in_progress?(app, area, env, tag, response)
    robot.trigger(:deploy_in_progress?, app: app, area: area, env: env, tag: tag, response: response)
  end

  def deploy_abort(payload)
    return payload[:response].reply(payload[:msg])
  end

  def deploy_app(app, env, tag, response_back, branch: nil, port: nil, drop_db: false, user_name:, target:)
    app_config = config.apps[app.to_sym]
    start_time = Time.now

    step_log("#{user_name} tries to deploy #{env} for #{app}")

    # Do not deploy if not in the right channel, only if channel is
    # set in config.
    allowed_channel = app_config[:channel]
    if user_name != 'Bitbucket' && allowed_channel && !allowed_room?(room_id, allowed_channel, log)
      return response_back.call("Deploy app #{app} permit only to ##{allowed_channel}")
    end

    file_logger = FileLogger.new(log, "deploy_#{app}_#{env}_#{start_time.strftime('%Y%m%d%H%M%S')}")
    # check env
    env_config, branch = \
    step :validate_deploy_request, :first_step do
      app_envs = app_config[:envs] ? app_config[:envs].keys : [:production] # use "production" as default env
      if !app_envs.include?(env.to_sym)
        return response_back.call(%{"#{env}" is not available env for #{app}, available env is: #{app_envs.join("|")}})
      end
      env_config = app_config[:envs] ? app_config[:envs][env.to_sym] : nil
      branch ||= env_config ? env_config[:branch] : 'master' # use "master" as default branch

      response_back.call("I'm deploying #{env} using #{branch} branch for #{app} ...")
      [env_config, branch]
    end

    allowed_auth_group = (env_config ? env_config[:auth_group] : nil) || app_config[:auth_group]
    if user_name != 'Bitbucket' && allowed_auth_group && !is_user_in_group(user_name, allowed_auth_group)
      return response_back.call("Deploy app #{app} permit only to users within ##{allowed_auth_group}")
    end

    robot.trigger(:deploy_started,
                  app: app, env: env, tag: tag, responsible: user_name, start_time: start_time)

    app_path, app_source_path, app_project_path, bundle_path = \
    step :prepare_tmp_dirs do
      # prepare the dirs
      app_path    = File.expand_path("/tmp/capistrano_rails/apps/#{app}")
      app_source_path = File.expand_path("#{app_path}/source")
      app_project_path = File.join(app_source_path, app_config[:subdir])
      bundle_path     = File.expand_path("tmp/capistrano_rails/bundle") # sharing bundle to reduce space usage and reuse gems
      FileUtils.mkdir_p(app_path)
      [app_path, app_source_path, app_project_path, bundle_path]
    end

    step :get_source_code do
      # if dir ".git" exists, use `git pull`
      if File.exists?("#{app_path}/source/.git")
        step_log 'Found ".git" exists, run `git pull`'
        # Utils::Service.run_in_dir("git checkout #{branch} && git pull", app_source_path)
        revision = "origin/#{branch}"
        # since we're in a local branch already, just reset to specified revision rather than merge
        Utils::Service.run_in_dir("git fetch #{verbose} && git reset #{verbose} --hard #{revision}", app_source_path, file_logger)
      else
        step_log 'not found ".git", run `git clone`'
        Utils::Service.run_in_dir("git clone #{app_config[:git]} source", app_path, file_logger)
      end

      # TODO: if ".git" exists but repo url changed, clean up the dir then run `git clone`
    end

    step :install_bundle_gems do
      result = Utils::Service.run_in_dir("bundle install --gemfile #{app_project_path}/Gemfile --path #{bundle_path} --deployment --without darwin development test", app_source_path, file_logger)
    end

    run_block = choose_cap_deploy(app_config, env, tag, response_back, branch: branch, port: port, drop_db: drop_db)
    step :run_cap_deploy do
      run_block.call(app_project_path, bundle_path, file_logger)
    end


    step :reply_deploy_success, :last_step do
      deploy_success = "deploy #{env} for #{app} finished!"

      # TODO: get "app_url"
      app_url = ""
      deploy_success += ", please visit #{app_url}" if !app_url.empty?
      fileurl = file_logger.finish
      if fileurl
        deploy_success += "\n You can download logs #{fileurl}"
      end

      response_back.call(deploy_success)
    end
  end

  finish_time =Time.now
  def response_success(result)
      # Send back a message indicating the deploy status
    if result[:exit_status] == 0 # could be 256
      robot.trigger(:deploy_finished, app: app, env: env, tag: tag, responsible: user_name,
                    start_time: start_time, finish_time: finish_time, status: 'success')
      msg_components = {title: "Finalize !", color: "good", text: ""}
    elsif result[:stderr].lines.last.include? "status code 32768"
      robot.trigger(:deploy_finished, app: app, env: env, tag: tag, responsible: user_name,
                      start_time: start_time, finish_time: finish_time, status: 'invalid tag')
      msg_components = {title: "A tag information not exists.", color: "warning", text: ""}
    else
      robot.trigger(:deploy_finished, app: app, env: env, tag: tag, responsible: user_name,
                    start_time: start_time, finish_time: finish_time, status: 'error')
      msg_components = {title: "Error!", color: "danger", text: output[:data]}
    end

      # generate the Attachment for slack
    attachment = Utils::Service.gen_deploy_msg(msg_components[:title], msg_components[:color], msg_components[:text],
                                               user_name, app, area, env, tag)

      # Default message for other adapters
    message = "Deploy - #{msg_components[:title]}. #{msg_components[:text]}"
    if (tag == "rollback")
      message = "Rollback - #{msg_components[:title]}. #{msg_components[:text]}"
    end

      case robot.config.robot.adapter
      when :slack
        return robot.chat_service.send_attachments(target, attachment)
      else
        robot.send_message(target, message)
      end
    end
  end

  def verbose
    # config.scm_verbose ? nil : "-q"
  end

  def choose_cap_deploy(app_config, env, tag, response_back, branch: nil, port: nil, drop_db: false)
    install_mode = app_config[:envs][env.to_sym][:install_mode] || app_config[:install_mode]
    env_variables = []
    env_variables << "BRANCH=#{branch}" if branch
    env_variables << "PORT=#{port}" if port
    env_variables << "DROP_DB=1" if drop_db
    case install_mode
    when 'capistrano'
      Proc.new do |app_project_path, bundle_path, log|
        # TODO: check "Capfile"
        # if "Capfile" not exits, reply "not a capistrano project"
        result = Utils::Service.run_in_dir("bundle exec cap #{env} deploy tag=#{tag} #{env_variables.join(' ')}", app_project_path, log)
        if
          fileurl = log.finish
          failure = %{errrors while executing: "#{result[:cmd]}"\n}
          failure << "\n You can download logs #{fileurl}\n" if fileurl
          failure << result[:stderr][0..200]
          step_fails(failure, response_back)
        end
      end
    when 'ssh'
      Proc.new do |app_project_path, bundle_path, log|
        result = Utils::Service.run_in_dir("#{env_variables.join(' ')} #{app_config[:envs][env.to_sym][:ssh_command]}", app_project_path, log)
        if result[:stderr]
          fileurl = log.finish
          failure = %{errrors while executing: "#{result[:cmd]}"\n}
          failure << "\n You can download logs #{fileurl}\n" if fileurl
          failure << result[:stderr][0..200]
          step_fails(failure, response_back)
        end
      end
  end
end
